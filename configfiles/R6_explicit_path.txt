ip explicit-path name R6R5R3R2R1
 next-address 10.5.6.5
 next-address 10.3.5.3
 next-address 10.2.3.2
 next-address 10.1.2.1

ip explicit-path name R6R5R4R2R1
 next-address 10.5.6.5
 next-address 10.4.5.4
 next-address 10.2.4.2
 next-address 10.1.2.1