---

## MPLS Traffic Engineering Automation

This repository contains Ansible playbooks and CI/CD pipeline configuration for automating the deployment and configuration of MPLS Traffic Engineering (TE), RSVP, and VRF forwarding loopbacks on network devices.

## Overview

This project aims to automate the configuration of MPLS Traffic Engineering (TE) on network devices using Ansible playbooks. The repository includes a series of playbooks to:
- Check device availability.
- Enable TE globally.
- Enable TE in the IGP.
- Enable RSVP on core devices.
- Create TE tunnels.
- Enable VRF forwarding loopbacks.

Additionally, a GitLab CI/CD pipeline is configured to automate the execution of these playbooks.

## Repository Structure

.
├── ansible.cfg
├── playbooks
│   ├── forwardingloopbacks.yml
│   ├── igp-te.yml
│   ├── mplste.yml
│   ├── ping.yml
│   ├── rsvp.yml
│   └── Te.yml
├── inventory
│   └── hosts
├── configfiles
│   ├── R1_explicit_path.txt
│   ├── R1_route_tunnel.txt
│   ├── R1_Te.txt
│   ├── R6_explicit_path.txt
│   ├── R6_route_tunnels.txt
│   └── R6_Te.txt
├── roles
│   ├── forwardingloopbacks
│   ├── igp_mplste
│   ├── mplste_global
│   ├── reachability
│   ├── rsvp
│   └── Te
├── log
│   └── ansible.log
└── .gitlab-ci.yml


## Explanation of `roles` Directory

- **forwardingloopbacks**: Role for configuring forwarding loopbacks.
- **igp_mplste**: Role for configuring IGP with MPLS TE.
- **mplste_global**: Role for enabling MPLS TE globally.
- **reachability**: Role for checking device reachability.
- **rsvp**: Role for configuring RSVP.
- **Te**: Role for configuring TE tunnels.

## Prerequisites

- GitLab Runner configured and connected to your GitLab instance.
- Ansible installed on the control node.
- SSH access to the target network devices with necessary credentials.
- Appropriate permissions to execute configurations on the network devices.

## Getting Started

### 1. Clone the Repository

sh
git clone https://gitlab.com/your-username/mpls-te-automation.git
cd mpls-te-automation


### 2. Set Up Inventory

Update the `inventory/hosts` file with your network devices.

### 3. Configure Ansible

The `ansible.cfg` file is pre-configured with necessary settings. Ensure the paths are correct and adjust settings as necessary.

### 4. Run Playbooks

You can manually run the playbooks using the following command:

sh
ansible-playbook -i inventory/hosts playbooks/<playbook_name>.yml


For example, to check device availability:

sh
ansible-playbook -i inventory/hosts playbooks/ping.yml


## Ansible Configuration

The `ansible.cfg` file contains the following settings:

ini
[defaults]
host_key_checking = False
inventory = /home/gitlab-runner/builds/kejPbWg7/0/kelvinwest32/mpls-te/inventory/hosts
log_path = /home/gitlab-runner/builds/kejPbWg7/0/kelvinwest32/mpls-te/log/ansible.log
deprecation_warnings = False
stdout_callback = yaml

[persistent_connection]
command_timeout = 180
connect_retry_timeout = 30


## CI/CD Pipeline

The CI/CD pipeline is defined in the `.gitlab-ci.yml` file and consists of several stages:

1. **Device Availability**: Checks if devices are reachable.
2. **Enabling TE Globally**: Enables MPLS TE globally.
3. **Enabling TE in IGP**: Configures TE within the IGP.
4. **Enabling RSVP on Core**: Enables RSVP on core devices.
5. **Enabling MPLS TE Tunnels**: Creates TE tunnels.
6. **Enabling VRF Forwarding Loopbacks**: Configures VRF forwarding loopbacks.

Each stage runs a specific Ansible playbook to perform the required configuration.

### Pipeline Configuration

The `.gitlab-ci.yml` file:

yaml
stages:
    - Development
    - Connectivity

.netdevops_template: &template
    tags:
        - netdevops

# stage 1
Device Availability:
  <<: *template
  stage: Development
  script:
    - ansible-playbook /home/gitlab-runner/builds/kejPbWg7/0/kelvinwest32/mpls-te/playbooks/ping.yml

# stage 1
Enabling TE GLOBALLY:
  <<: *template
  stage: Development
  needs: ["Device Availability"]
  script:
    - ansible-playbook /home/gitlab-runner/builds/kejPbWg7/0/kelvinwest32/mpls-te/playbooks/mplste.yml

# stage 2
Enabling TE IN IGP:
  <<: *template
  stage: Development
  needs: ["Enabling TE GLOBALLY"]
  script:
    - ansible-playbook /home/gitlab-runner/builds/kejPbWg7/0/kelvinwest32/mpls-te/playbooks/igp-te.yml

# stage 3
Enabling RSVP ON CORE:
  <<: *template
  stage: Development
  needs: ["Enabling TE IN IGP"]
  script:
    - ansible-playbook /home/gitlab-runner/builds/kejPbWg7/0/kelvinwest32/mpls-te/playbooks/rsvp.yml

# stage 4
Enabling MPLS TE TUNNELS:
  <<: *template
  stage: Development
  needs: ["Enabling RSVP ON CORE"]
  script:
    - ansible-playbook /home/gitlab-runner/builds/kejPbWg7/0/kelvinwest32/mpls-te/playbooks/Te.yml

# stage 5
Enabling VRF FORWARDING LOOPBACKS:
  <<: *template
  stage: Development
  needs: ["Enabling MPLS TE TUNNELS"]
  script:
    - ansible-playbook /home/gitlab-runner/builds/kejPbWg7/0/kelvinwest32/mpls-te/playbooks/forwardingloopbacks.yml


## Contributing

Contributions are welcome! Please submit a merge request or open an issue to discuss potential changes.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

---
