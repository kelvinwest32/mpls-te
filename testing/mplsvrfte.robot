*** Settings ***
Library        pyats.robot.pyATSRobot 
Library        genie.libs.robot.GenieRobot  
Library        unicon.robot.UniconRobot
Library        OperatingSystem

*** Variables ***
${testbed}     coredevices.yaml

*** Test Cases ***
Connectivity to Pe1
    pyats.robot.pyATSRobot.Use Testbed "${testbed}"
    connect to all devices

Check MPLS TE Tunnels on Pe1
    ${output}=  execute "show mpls traffic-eng tunnels" on device "R1"
    Should Contain  ${output}    Name: R1_t0 Name: R1_t1

Check MPLS TE Tunnels on Pe6
    ${output}=  execute "show mpls traffic-eng tunnels" on device "R1"
    Should Contain  ${output}    Name: R6_t0 Name: R6_t1